module AppointmentHelper

  # formata o link de acordo com a data, verificando se ja esta agendado
  def format_link(hour, day)
    date = day.change({ hour:hour})
    
    link = "<a href=# id=reserve data-id=#{hour}-#{day.day} data-user='#{current_user.name}' data-message=#{day.change({ hour:hour})}>"
    appointment = Appointment.where(:date => date).first
    
    return link.concat("Disponível</a>").html_safe unless appointment.present?

    if appointment.user == current_user
      link = "<a href=# id=release data-id=#{hour}-#{day.day} data-appointmentid=#{appointment.id} data-message=#{day.change({ hour:hour})}>#{appointment.user.name}</a>"

      return link.html_safe
      
    else
      return appointment.user.name
    end

  end

  def format_hour(hour)
    return "#{hour}:00"
  end
end
