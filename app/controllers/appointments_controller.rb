class AppointmentsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json, :html 

  def index

    time = DateTime.now

    @monday = time.beginning_of_week 
    @tuesday = time.beginning_of_week + 1.day
    @wednesday = time.beginning_of_week + 2.days
    @thursday = time.beginning_of_week + 3.days
    @friday = time.beginning_of_week + 4.days

    @appointments = Appointment.thisweek

    respond_with(@appointments)
  end

  def create

    @appointment = Appointment.create(:user_id => current_user.id, 
                            :date => params[:date])
    respond_with(@appointment)
 
  end

  def destroy

    @appointment = Appointment.find(params[:id])
    @appointment.destroy
    
    respond_with(@appointment)
  end

end
