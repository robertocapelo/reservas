class Appointment < ActiveRecord::Base
  belongs_to :user
  validates :date, presence: true
  validates :user, presence: true

  scope :thisweek, -> { where(:date => DateTime.now.beginning_of_week..DateTime.now.end_of_week) } 
end
