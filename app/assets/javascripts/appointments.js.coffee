$(document).ready ->
  $('a[id^="reserve"]').click ->
    
    user = this.getAttribute("data-user")
    datamessage = this.getAttribute("data-message")
    link = this.getAttribute("data-id")

    $.ajax
      url: '/appointments'
      type: 'POST'
      data: 
        date: datamessage
      success: (data, status, response) ->
        $("[data-id="+link+"]")[0].text = user
        $("[data-id="+link+"]")[0].removeAttribute('id')
        $("[data-id="+link+"]")[0].setAttribute("id", "release");
        $("[data-id="+link+"]")[0].setAttribute("data-appointmentid", data.id);
        
        window.location.reload();
    false

  $('a[id^="release"]').click ->
    link = this.getAttribute("data-id")
    appointmentid = this.getAttribute("data-appointmentid")
    $.ajax
      url: '/appointments/'+ appointmentid
      type: 'DELETE'
      data:
        data: this.getAttribute("data-id")
      success: (data, status, response) ->
        $("[data-id="+link+"]")[0].text = 'Disponível'
        $("[data-id="+link+"]")[0].removeAttribute('id')
        $("[data-id="+link+"]")[0].setAttribute("id", "reserve");
        $("[data-id="+link+"]")[0].removeAttribute('data-appointmentid');
        window.location.reload();
    false