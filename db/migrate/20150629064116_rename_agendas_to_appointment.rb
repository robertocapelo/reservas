class RenameAgendasToAppointment < ActiveRecord::Migration
  def change
    rename_table :agendas, :appointments
  end
end
