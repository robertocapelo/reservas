class CreateAgendas < ActiveRecord::Migration
  def change
    create_table :agendas do |t|
      t.datetime :data
      t.belongs_to :user
      
      t.timestamps
    end
  end
end
