class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :appointments, :data, :date
  end
end
