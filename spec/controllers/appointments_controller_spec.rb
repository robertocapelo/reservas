require 'rails_helper'

RSpec.describe AppointmentsController, type: :controller do
  describe "GET index" do
    before(:each) do
      sign_out :user
    end
    
    it "get to a redirect to login" do
      sign_out :user
      get :index
      expect(response).to redirect_to(new_user_session_url)
    end

    it "creates a day in agenda" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = FactoryGirl.create(:user)

      sign_in :user, @user
      post :create, :format => :json, :date => "2015-06-26 10:00:00"
      
      expect(response.status).to eq(201) 
    end

    it "release a day in agenda" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = FactoryGirl.create(:user)

      sign_in :user, @user
      post :create, :format => :json, :date => "2015-06-26 10:00:00"
      
      delete :destroy, :format => :json, :id => "1"
  
      expect(response.status).to eq(204) 
    end
  end
end
