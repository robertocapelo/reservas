require 'rails_helper'

RSpec.describe Appointment, type: :model do
  
  it "has a valid factory" do
    expect(FactoryGirl.create(:appointment)).to be_valid
  end

  it "is invalid without a data" do
    expect(FactoryGirl.build(:appointment, date: nil)).not_to be_valid
  end
  
  it "is invalid without an user" do
    expect(FactoryGirl.build(:appointment, user: nil)).not_to be_valid
  end
end
