FactoryGirl.define do 
  factory :appointment do |f| 
    f.date "2015-06-23 10:00:00"
    association :user, factory: :user
  end 
end 